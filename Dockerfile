FROM linuxserver/nginx:latest

WORKDIR /config/www

COPY ./html .

WORKDIR /config/www/img

COPY ./resume/resume.pdf .