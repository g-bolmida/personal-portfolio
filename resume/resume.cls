% Imports and doc settings
\ProvidesClass{resume}[resume]
\LoadClass[10pt,letterpaper]{article}

\usepackage[parfill]{parskip}
\usepackage{array} 
\usepackage{ifthen}
\pagestyle{empty}

% Doc styling
\def \title#1{\def\@title{#1}}
\def \@title {}

\def \contact#1{\def\@contact{#1}}
\def \@contact {}

\def \gitLinks#1{\def\@gitLinks{#1}}
\def \@gitLinks {}

\def \dispTitle {
  \begingroup
    \hfil{\MakeUppercase{\huge\bf \@title}}\hfil
    \smallskip\break
  \endgroup
}

\def \dispContact {
  \begingroup
    \def \\ {$\bullet$ }
    \@contact
  \endgroup
  \smallskip
}

\def \dispGitLinks {
  \begingroup
    \def \\ {$\bullet$ }
    \@gitLinks
  \endgroup
  \smallskip
}

\let\ori@document=\document
\renewcommand{\document}{
  \ori@document
  \dispTitle
  \centerline{\dispContact}
  \centerline{\dispGitLinks}
}

\newenvironment{block}[1]{
  \MakeUppercase{\bf #1}
  \medskip
  \hrule
  \begin{list}{}{
    \setlength{\leftmargin}{1.5em}
  }
  \item[]
}{
  \end{list}
}

\newenvironment{subBlock}[4]{
 {\bf #1} \hfill {#2}
 \ifthenelse{\equal{#3}{}}{}{
  \\
  {\em #3} \hfill {\em #4}
  }\smallskip
  \begin{list}{\tiny$\bullet$}{\leftmargin=0em}
   \itemsep -0.5em \vspace{-0.8em}
  }{
  \end{list}
  \vspace{0.5em}
}
