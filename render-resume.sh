#!/bin/bash

docker run --rm -v "$PWD"/resume:/data --entrypoint pdflatex pandoc/latex:latest ./resume.tex